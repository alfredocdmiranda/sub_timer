# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [0.1] - 2016-08-15
### Added
- Can delay a SRT file
- Can advance a SRT file
- Can advance or delayer only a range of tracks
- Implemented CLI interface
