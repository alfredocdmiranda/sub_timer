import sys
import argparse

__author__ = 'Alfredo Miranda'
__version__ = '0.2'

class Time(object):
    def __init__(self, *args, **kwargs):
        self.hours = args[0]
        self.minutes = args[1]
        self.seconds = args[2]
        self.miliseconds = args[3]

    @classmethod
    def strftime(cls, str_time):
        time_split = str_time.replace(" ","").split(":")
        hours = int(time_split[0])
        minutes = int(time_split[1])
        seconds = int(time_split[2].split(",")[0])
        miliseconds = int(time_split[2].split(",")[1])

        return cls(hours,minutes,seconds,miliseconds)

    def __add__(self, other):
        miliseconds = (self.miliseconds + other) % 1000
        value = (self.miliseconds + other) // 1000
        if value > 0:
            seconds = (self.seconds + value) % 60
            value = (self.seconds + value) // 60
            if value > 0:
                minutes = (self.minutes + value) % 60
                value = (self.minutes + value) // 60
                if value > 0:
                    hours = self.hours + value
                else:
                    hours = self.hours
            else:
                minutes = self.minutes
                hours = self.hours
        else:
            seconds = self.seconds
            minutes = self.minutes
            hours = self.hours

        return Time(hours,minutes,seconds,miliseconds)

    def __sub__(self, other):
        miliseconds = (self.miliseconds - other) % 1000
        value = (self.miliseconds - other) // 1000
        if value < 0:
            seconds = (self.seconds + value) % 60
            value = (self.seconds + value) // 60
            if value < 0:
                minutes = (self.minutes + value) % 60
                value = (self.minutes + value) // 60
                if value < 0:
                    hours = self.hours + value
                    if hours < 0:
                        raise ValueError("Time can't be negative.")
                else:
                    hours = self.hours
            else:
                minutes = self.minutes
                hours = self.hours
        else:
            seconds = self.seconds
            minutes = self.minutes
            hours = self.hours

        return Time(hours,minutes,seconds,miliseconds)

    def __repr__(self):
        return '{:02d}:{:02d}:{:02d},{:03d}'.format(self.hours,self.minutes,
                                                    self.seconds,self.miliseconds)

class Track(object):
    def __init__(self, pos, time, text):
        self.pos = pos
        self.start, self.end = self.split_time(time)
        self.text = ''
        for t in text:
            self.text += t
            self.text += '\n'

    def split_time(self, time):
        time_splitted = time.split("-->")
        start = Time.strftime(time_splitted[0])
        end = Time.strftime(time_splitted[1])

        return start,end

    def __repr__(self):
        return "{}\n{} --> {}\n{}".format(self.pos,self.start,self.end,self.text)

class Subtitle(object):
    def __init__(self, tracks):
        self.tracks = tracks
    
    @classmethod
    def read_srt(self, data):
        tracks = []
        track = []
        for line in data:
            if not line:
                tracks.append(track)
                track = []
            else:
                track.append(line)
        
        if track:
            tracks.append(track)
        
        tracks_obj = []
        for t in tracks:
            if t:
                tracks_obj.append(Track(t[0],t[1],t[2:]))

        return Subtitle(tracks_obj)
    
    def backward(self, offset, _tracks=slice(0,None)):
        for index,t in enumerate(self.tracks[_tracks],start=_tracks.start):
            self.tracks[index].start -= offset
            self.tracks[index].end -= offset
    
    def forward(self, offset, _tracks=slice(0,None)):
        for index,t in enumerate(self.tracks[_tracks],start=_tracks.start):
            self.tracks[index].start += offset
            self.tracks[index].end += offset
    
    def write_srt(self):
        data = ""
        for track in self.tracks:
            data += str(track) + "\n"
        
        return data

def arguments():
    global args

    parser = argparse.ArgumentParser(description='It delays or advance SRT files.')
    parser.add_argument('filename', help='Input SRT file.')
    parser.add_argument('offset', type=int, help='Offset to be used to delay or advance. To delay, you must use negative offset.')
    parser.add_argument('-o', '--output', default='output.srt', help='Output SRT file.')
    parser.add_argument('-t', '--tracks', default="1:", help='Choose a range of tracks to be modified. e.g. 100:200')
    args = parser.parse_args()

    if args.tracks:
        start,end = args.tracks.split(":")
        start = int(start)-1 if start else 0
        end = int(end) if end else None
        args.tracks = slice(start,end)
    else:
        args.tracks = slice(0,None)

if __name__ == '__main__':
    arguments()

    with open(args.filename) as f:
        text = f.readlines()
        text = [line.replace("\r","").replace("\n","") for line in text]

    srt = Subtitle.read_srt(text)

    if args.offset >= 0:
        srt.forward(args.offset, args.tracks)
    else:
        srt.backward(args.offset*(-1), args.tracks)
    
    srt_data = srt.write_srt()

    with open(args.output, 'w') as f:
        f.write(srt_data)
